
// Общие данные про состояние кофемании. У нас несколько плугинов, поэтому
// для шаринга

var CoffemaniaSharedData = {};
CoffemaniaSharedData.MOBILE_WIDTH = 1000;
CoffemaniaSharedData.prevWidth = $(window).width();


// Coffemania FM Angular App

angular.module("CoffemaniaFMApp",[])
	.value("RadioTochkaAPIUrl", "http://coffeemania.fm:8080/")
	.directive("volume", ["$rootScope", function($rootScope) {
		return function($scope, element) {
			// при нажатии делаем драг
			var isdragging = false;
			var oldx = undefined;
			$scope.volume = (window["localStorage"] ? (localStorage.getItem("CoffeemaniaFM-volume") ? parseFloat(localStorage.getItem("CoffeemaniaFM-volume")) : 0.8) : 0.8);
			$rootScope.$broadcast("volume", { data: $scope.volume });

			element.find(".pin").on("mousedown", function(event) {
				isdragging = true;
				oldx = event.screenX;
				turnOffSelection();
			});

			element.on("click", "a.icon-soundoff", function() {
				$scope.volume = 0.9;
				applyChanges();
			});

			element.on("click", "a.icon-soundon, a.icon-soundhalf", function() {
				$scope.volume = 0;
				applyChanges();
			});

			$(document).on("mousemove", function(event) {
				if(isdragging) {
					var step = (event.screenX - oldx);
					oldx = event.screenX;
					$scope.volume = $scope.volume + (step / element.width());
					if($scope.volume < 0.0001) {
						$scope.volume = 0;
					} else if($scope.volume > 0.9999) {
						$scope.volume = 0.9999;
					}
					applyChanges();
				}
			});

			function applyChanges() {
				$scope.$apply();
				$rootScope.$broadcast("volume", { data: $scope.volume });
				if(window["localStorage"]) {
					localStorage.setItem("CoffeemaniaFM-volume", $scope.volume)
				}
			}

			$(document).on("mouseup", function() {
				isdragging = false;
				turnOnSelection();
			});

			$(document).on("mouseleave", "body", function() {
				isdragging = false;
				turnOnSelection();
			});

			function turnOffSelection() {
				$("body").addClass("no-select");
			}

			function turnOnSelection() {
				$("body").removeClass("no-select");
			}
		}
	}])
	.directive("coffeemaniaRadio", ["$rootScope", function($rootScope) {
		return function($scope, element) {

			$scope.$on("create-radio", function(event, data) {
				var audio = element.find("audio");
				if(data.data.autoplay) {
					audio.attr("autoplay", "true");
					audio[0].pause();
					audio.attr("src", data.data.url);
					audio[0].load();
					audio[0].play();
				} else {
					element.children().remove();
					audio = $("<audio src='"+data.data.url + "'>");
					element.append(audio);
					audio[0].volume = data.data.volume;
				}
			});

			$scope.$on("play-radio", function() {
				element.find("audio").get(0).load();
				element.find("audio").get(0).play();
			});

			$scope.$on("pause-radio", function() {
				element.find("audio").get(0).pause();
			});

			$scope.$on("volume", function(event, eventData) {
				element.find("audio").get(0).volume = eventData.data;
			});

		}
	}])
	.controller("CoffemaniaFMCtrl", ["$scope", "$http", "$q", "RadioTochkaAPIUrl", "$sce", "$timeout", "$rootScope", function($scope, $http, $q, RadioTochkaAPIUrl, $sce, $timeout, $rootScope) {

		// задник
		$scope.siteBack = "url(" + (location.href.search("localhost") > -1 || location.href.search("mr-woodman.ru") > -1 || location.href.search("herokuapp.com") > -1 ? "" : "/" ) + "images/back-album-cover-unknown.jpg)";

		// Отслеживаем текущую позицию меню
		$scope.currentPageId = 0;
		$scope.$on("changed.owl.carousel", function(e, data) {
			$scope.currentPageId = data.currentPageId;
			$scope.currentPageTitle = data.currentPageTitle;
			$scope.$apply();
		});

		// Отслеживаем мобильное или нет
		$scope.isMobile = false;
		$scope.$on("toMobile", function() {
			$scope.isMobile = true;
			$scope.$apply();
		});
		$scope.$on("toDesktop", function() {
			$scope.isMobile = false;
			$scope.$apply();
		});

		// mеню
		$scope.menuHidden = true;
		$scope.currentPageTitle = "Сейчас играет";
		$scope.toggleMenu = function() {
			$scope.menuHidden = !$scope.menuHidden;
		}

		// Левый бар
		$scope.isLeftBarHidden = true;
		$scope.toggleLeftBar = function() {
			$scope.isLeftBarHidden = !$scope.isLeftBarHidden;
		}


		// карта
		$scope.mapURL = $sce.trustAsResourceUrl("http://coffeemania.fm/gmaps.php");
		function reloadMap() {
			var old = $scope.mapURL;
			$scope.mapURL = $sce.trustAsResourceUrl("http://coffeemania.fm/empty.html");
			$timeout(function() {
				$scope.mapURL = old;
			},1)
		}


		/*
			Сначала надо получить адреса аудио-потоков (и названия их)
			И уметь обновить

		*/

		$scope.radio = {
			channels: [],
			servers: [],
			current: 0,
			currentUrl: "",
			program: [],
			paused: true,
			volume: 0.8
		};

		$scope.currentSongForOrder = 0;

		$scope.setCurrentSongForOrder = function(song) {
			$scope.currentSongForOrder = $scope.radio.program.indexOf(song);
			// $timeout(function() {
			// 	refreshIScroll();
			// },1000);
		}

		//var radioElement = document.getElementById("coffemania-fm-radio");


		// Фильтруем список серверов и каналов (удаляем лишние)
		function filterChannels(channels, servers) {
			var removeUri = [];
			angular.forEach(servers, function (item) {
				// вот кандидат на удаление
				if (item.title === "stream") {
					// добавляем в список для удаления
					removeUri.push(item.resource_uri);
				}
			});
			// удаляем по списку
			angular.forEach(removeUri, function (uri) {
				angular.forEach(servers, function (server, id) {
					if (server.resource_uri === uri) {
						servers.splice(id, 1);
					}
				});
				angular.forEach(channels, function (channel, id) {
					if (channel.server === uri) {
						channels.splice(id, 1);
					}
				});
			});
		}

		$scope.radio.refreshChannelsList = function(initialChannel) {
			$http.jsonp(RadioTochkaAPIUrl+"api/channels/?callback=JSON_CALLBACK").then(function(result) {
				$scope.radio.channels = result.data.objects;
				$scope.radio.channels.reverse(); // в обратную сторону
				// так же возьмем названия
				$http.jsonp(RadioTochkaAPIUrl+"api/servers/?callback=JSON_CALLBACK").then(function(result) {
					$scope.radio.servers = result.data.objects;
					$scope.radio.servers.reverse(); // в обратную сторону
					// Профильтровать список серверов (исключить из списка stream сервер)
					filterChannels($scope.radio.channels, $scope.radio.servers);
					// поставим первый канал из списка
					$scope.radio.setChannel(((initialChannel != undefined) ? initialChannel : 0));
				});
				// запускаем также обновлялку списка истории/программы
				//$scope.radio.startTrackProgram();

			}, function(error) {
				// error
			})
		}

		// ставим канал из списка. собственное айди канала - это другое, лежит в инфе канала
		$scope.radio.setChannel = function(id) {
			$scope.radio.current = id;
			var info = $scope.radio.channels[$scope.radio.current];
			//$scope.radio.currentUrl = $sce.trustAsResourceUrl("http://" + info.host + ":" + info.port + info.mount_point);
			$rootScope.$broadcast("create-radio", {
				data: {
					url: $sce.trustAsResourceUrl("http://" + info.host + ":" + info.port + info.mount_point),
					autoplay : !$scope.radio.paused,
					volume: $scope.radio.volume
				}
			});
			$scope.currentSongForOrder = 0;
			// должны получить данные по истории/программе канала
			getProgram();
			// и перезагрузить карту
			reloadMap();
			// запомнить
			if(window["localStorage"]) {
				localStorage.setItem("CoffeemaniaFM-channel", id);
			}
		}

		$scope.radio.toggle = function() {
			if($scope.radio.paused) {
				$scope.radio.play();
			} else {
				$scope.radio.pause();
			}
		}

		$scope.radio.play = function() {
			$rootScope.$broadcast("play-radio");
			// console.log("play");
			$scope.radio.paused = false;
		}

		$scope.radio.pause = function() {
			$rootScope.$broadcast("pause-radio");
			// console.log("pause");
			$scope.radio.paused = true;
		}

		$scope.radio.getDuration = function(song) {
			var ms = song.track_length_ms;
			//var id = $scope.radio.program.indexOf(song);
			var min = parseInt(ms / 60000);
			var sec = parseInt((ms - min*60000)/1000);
			var time = min + ":" + ("0"+sec).slice(-2);
			return time;
		}

		$scope.$on("volume", function(event, eventData) {
			$scope.radio.volume = eventData.data;
		});


		var isTrackingProgram = true;
		var trackProgramDelay = 60000;
		var programTimeout;

		function getProgram() {
			var channel = $scope.radio.channels[$scope.radio.current].id;
			$http.jsonp(RadioTochkaAPIUrl+"api/v2/forecast/?format=jsonp&server=" + channel + "&with_current=1&callback=JSON_CALLBACK").then(function(result) {
				$scope.radio.program = result.data;
				// console.log(result.data[0]);
				getEstimated();
				// get album art
				getAlbumArt($scope.radio.program[0]);
				//делаем партнерскую ссылку на альбом на iTunes
				makeItunesAlbumLink($scope.radio.program[0]);
				//$(".iscroll-wrapper").data("iscroll-api").refresh();
			});
			// console.log("track program");
			if(isTrackingProgram) {
				if(programTimeout) {
					$timeout.cancel(programTimeout);
				}
				programTimeout = $timeout(function() {
					getProgram();
				}, trackProgramDelay)
			}
		}

		function getCurrentSong() {

		}

		var estimatedTimeout;
		var trackEstimatedDelay = 700;

		var timezone = window.server_time_zone_shift ? window.server_time_zone_shift : "+0300";

		function getEstimated() {
			var song = $scope.radio.program[0];
			var start = new Date(song.start_time+timezone);
			if(navigator.userAgent.search("Safari") > -1) {
				var dir = (timezone[0] == "-" ? -1 : 1);
				var h = parseInt(timezone.substr(1,2));
				var m = parseInt(timezone.substr(3,2));
				start = new Date(song.start_time+"Z");
				start.setUTCHours(start.getUTCHours()-h);
				start.setUTCMinutes(start.getUTCMinutes()-m)
			}
			var length = song.track_length_ms;


			var now = new Date();
			var estimated = start.getTime() + length - now.getTime();
			// fix win7
			if(estimated > 3600000) {
				estimated = estimated - 3600000;
			}
			$scope.radio.currentEstimatedPercent = (1 - (estimated/length)) * 100;
			if($scope.radio.currentEstimatedPercent < 0) {
				$scope.radio.currentEstimatedPercent = 0;
			}
			// console.log("длина песни", start, now, song.start_time);
			$scope.radio.currentEstimatedTime = "-"+$scope.radio.getDuration({track_length_ms: estimated});
			if(estimated < 0 && estimated > -180000) {
				getProgram();
			}
			if(estimatedTimeout) {
				$timeout.cancel(estimatedTimeout);
			}
			estimatedTimeout = $timeout(function() {
				getEstimated();
			}, trackEstimatedDelay)
		}


		var initChannel = window["localStorage"] ? parseInt(localStorage.getItem("CoffeemaniaFM-channel") ? localStorage.getItem("CoffeemaniaFM-channel") : 0) : undefined;
		$scope.radio.refreshChannelsList(initChannel);

		//делаем партнерскую ссылку на альбом на iTunes
		function makeItunesAlbumLink(song) {
			//Affiliate Token для партнерской ссылки
			var AffiliateToken = '11lSrA';

			//определяем язык страницы
			var country = 'ru';
			if( window.location.pathname == '/en') country = 'en';

			//партнерская ссылка на альбом на iTunes
			//По умолчанию (если не найдем ссылку альбом) делаем ссылку на главную страницу магазина музыки
			var linkUrl = '';
			if( country == 'ru' )
				linkUrl = 'https://itunes.apple.com/ru/';
			else
				linkUrl = 'https://itunes.apple.com/gb/';

			//добавляем партнерский код
			linkUrl = linkUrl + '?at=' + AffiliateToken;

			//вставляем ссылку по умолчанию на страницу
			$("#ituneslink").attr("href", linkUrl);

			//Делаем ajax-запрос для получения ссылки н альбом
			$.ajax({
			  type: "POST",
			  url: "/getID3/demos/make-itunes-album-url.php",
			  data: {artist: song.artist, track: song.title, country: country },
			  async: true,
			  success: function(AlbumURL){
				//добавляем партнерский код
				if( AlbumURL != 0)
					linkUrl = AlbumURL + '&at=' + AffiliateToken;
				//debug
				// console.log( "Сделана ссылка на альбом: " + linkUrl );
				//вставляем ссылку на страницу
				$("#ituneslink").attr("href", linkUrl);
			  },
			  error: function(xhr, status, error) {
				// console.log('Error getting iTunes link: '+xhr.responseText);
			  }

			});
		}

		// вытаскиваем обложки
		var lastfm = new LastFM({
			apiKey    : 'a9277d9f34adc94ffd7cee8e7c1ee844',
			apiSecret : 'bab9acb83a5f2f71942f40b73a4f7de3'
			//cache     : cache
		});

		$scope.currentAlbumArt = null;
		window.lastfm = lastfm;

		// // адрес картинки на локальном сервере
		// var imageURL = false;

		// function getAlbumArt_local_url(artist, track) {
		// 	//imageURL = false;
		// 	//var request_data = {artist: artist, track: track };

		// 	$.ajax({
		// 	  type: "POST",
		// 	  url: "http://coffeemania.fm/getID3/demos/get-album-image.php",
		// 	  //data: "artist="+artist+"&track="+track,
		// 	  //data: '{"artist":'+'"'+artist+'"'+', "track":'+'"'+track+'"'+'}',
		// 	  data: {artist: artist, track: track },
		// 	  async: false,
		// 	  success: function(msg){
		// 		//alert( "Прибыли данные: " + msg );
		// 		if( msg != '')
		// 			imageURL = msg;
		// 		else
		// 			imageURL = false;
		// 	  },
		// 	  error: function(xhr, status, error) {
		// 		imageURL = false;
		// 		console.log('Error: '+xhr.responseText);
		// 	  }

		// 	});
		// 	return imageURL;
		// }

		function getAlbumArtFromCoffeemania (artist, track) {
			var deferred = $q.defer();
			$http({
				method: "POST",
				url: "http://coffeemania.fm/getID3/demos/get-album-image.php",
				data: { artist: artist, track: track }
			}).then(function (result) {
				// console.log(typeof(result), result, "ZZZZZZZZZZZZZZZZZZZZZZZZZZz");
				if (result && typeof(result) == "string") {
					deferred.resolve(result);
				} else {
					deferred.reject(result);
				}
			}, function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}

		function getPriority (image) {
			if (!image["#text"]) {
				return 0;
			}
			switch (image.size) {
				case "mega":
					return 1;
				case "medium":
					return 2;
				case "large":
					return 3;
				case "extralarge":
					return 4;
				default:
					return 0;
			}
		}

		function parseImages (arr) {
			if (arr.length > 0) {
				arr.sort(function (a, b) {
					return getPriority(b) - getPriority(a);
				});
				if (arr[0]["#text"]) {
					return arr[0]["#text"];
				}
			}
		}

		function getArtistImages(artist) {
			var deferred = $q.defer();
			lastfm.artist.getInfo({ artist: artist }, {
				success: function (data) {
					var biggestImage = parseImages(data.artist.image);
					if (biggestImage) {
						deferred.resolve(biggestImage);
					} else {
						deferred.reject();
					}
				},
				error: function (code, error) {
					deferred.reject();
				}
			});
			return deferred.promise;
		}

		function getAlbumArt(song) {
			lastfm.track.getInfo({artist: song.artist, track: song.title }, {
				success: function(data) {
					if (data.track.album) {
						var biggestImage = parseImages(data.track.album.image);
// console.log("lastfm album", biggestImage);
						if (biggestImage) {
							// если есть обложка от last.fm
							$scope.currentAlbumArt = $sce.trustAsResourceUrl(biggestImage);
						} else {
							getAlbumArtFromCoffeemania(song.artist, song.title).then(function (url) {
// console.log("coffemania album", url);
								$scope.currentAlbumArt = $sce.trustAsResourceUrl(url);
							}, function (error) {
								// Берем фотку исполнителя
								getArtistImages(song.artist).then(function (url) {
// console.log("artist image", url);
									$scope.currentAlbumArt = $sce.trustAsResourceUrl(url);
								},
								function (error) {
									// если нет, очищаем
// console.log("nothing art album", error);
									$scope.currentAlbumArt = null;
								});
							});
						}
					} else {
						// очищаем
// console.log("nothing art album");
						$scope.currentAlbumArt = null;
					}
					$scope.$apply();
				},
				error: function(code, error) {
					// берем обложку с локального сервера
					getAlbumArtFromCoffeemania(song.artist, song.title).then(function (url) {
						// debug
						// console.log("err coffemania album", url);
						$scope.currentAlbumArt = $sce.trustAsResourceUrl(url);
					}, function (error) {
						// Берем фотку исполнителя
						getArtistImages(song.artist).then(function (url) {
							// debug
							// console.log("err artist image", url);
							$scope.currentAlbumArt = $sce.trustAsResourceUrl(url);
						},
						function (error) {
							// debug
							// console.log("nothing art album", error);
							// если нет, очищаем
							$scope.currentAlbumArt = null;
						});
					});
					$scope.$apply();
				}
			});
		}

	}]);


// jquery onReady события

$(document).on("ready", function() {

	// врубаем angular
	angular.element(document).ready(function() {
	  CoffemaniaSharedData.angularScope = angular.bootstrap(document, ['CoffemaniaFMApp']).get("$rootScope");
	});

	// проверяем девайсы
	if(navigator.userAgent.match(/Android|BlackBerry|iPad|iPod|iPhone|Opera Mini|IEMobile/i)) {
		$("body").addClass("mobile-device")
	}

	// настраиваем переключение меню
	var firstLoaded = true;
	var pages = $("#site-content").eq(0);
	pages.on("changed.owl.carousel", function(e) {
		var currentPageId = e.item.index ? e.item.index : 0;

		if(firstLoaded == false) {
			location.href = location.href.split("#")[0] + "#" + $("#site-content article").eq(currentPageId).attr("data-hash");
		}
		if(firstLoaded) firstLoaded = false;

		$("#site-menu a").each(function() {
			if($(this).index() == currentPageId) {
				$(this).addClass("active");
			} else {
				$(this).removeClass("active");
			}
		});

		$("body, html, #site-content").animate({
			scrollTop: 0
		}, 250);

		forceRedraw();

		// а также посылаем событие в angular модуль
		CoffemaniaSharedData.angularScope.$broadcast("changed.owl.carousel", {
			"currentPageId" : currentPageId,
			"currentPageTitle": $("#site-menu a.button-on-active").eq(currentPageId).text()
		});
	});
	pages.owlCarousel({
        items: 1,
        URLhashListener: true,
        autoHeight: true,
        mouseDrag: false,
        touchDrag: false,
        navigation: false,
        startPosition: 'URLHash',
        animateIn: "fadeIn",
        margin: 2,
        onAfter: function() {
        	forceRedraw();
        }
    });

 //    createIScroll(".iscroll-wrapper");

	// // чтоб можно было выделять текст
	// $(".iscroll-wrapper").on("mousedown", "*", function() {
	// 	if($(window).width() >= CoffemaniaSharedData.MOBILE_WIDTH) {
	// 		$(this).parents(".iscroll-wrapper").data("iscroll-api").disable();
	// 	}
	// });
	// $(".iscroll-wrapper").on("mouseup", "*", function() {
	// 	if($(window).width() >= CoffemaniaSharedData.MOBILE_WIDTH) {
	// 		$(this).parents(".iscroll-wrapper").data("iscroll-api").enable();
	// 	}
	// });


	if($(window).width() >= CoffemaniaSharedData.MOBILE_WIDTH) {
		// ставим контент текущей песни по центру
		resizeNowPlayingArticle();
		toDesktop();
	} else {
		toMobile();
	}


	// Баннер установки приложения
	var lang = $("html").attr("lang");
	new SmartBanner({
		title: "Coffeemania FM",
		author: "Coffeemania",
		daysHidden: 0,
		daysReminder: 0,
		// force: "ios",
		button: (lang == "ru" ? "Смотреть" : 'View'),
		store: {
			ios: "App Store",
			android: (lang == "ru" ? "в Google Play" : 'in Google Play'),
			windows: (lang == "ru" ? "в Windows Store" : 'in Windows Store')
		},
		price: {
			ios: (lang == "ru" ? "Цена: БЕСПЛАТНО в" : 'Price: FREE on the'),
			android: (lang == "ru" ? "ЗАГРУЗИТЬ" : 'Free'),
			windows: (lang == "ru" ? "ЗАГРУЗИТЬ" : 'Free')
		}
	});
});

// вещи после загрузки картинок

$(window).on("load", function() {

	// превращаем картинки svg в инлайновые
	$("img[src$='.svg']").each(function() {
		var img = $(this);
		$.get(img.attr("src"), function(result) {
			var svg = $(result).find("svg");
			svg.attr("class", img.attr("class"));
			img.after(svg);
			img.remove();
		})
	});

    refreshArticleSize();
    //refreshIScroll();

    $(window).trigger("resize");
});

// вещи, случающиеся при ресайзе
$(window).on("resize", function() {
	var w = $(window).width();
	if(CoffemaniaSharedData.prevWidth < CoffemaniaSharedData.MOBILE_WIDTH && w >= CoffemaniaSharedData.MOBILE_WIDTH) {
		CoffemaniaSharedData.prevWidth = w;
		toDesktop();
	} else if(CoffemaniaSharedData.prevWidth >= CoffemaniaSharedData.MOBILE_WIDTH && w < CoffemaniaSharedData.MOBILE_WIDTH) {
		CoffemaniaSharedData.prevWidth = w;
		toMobile();
	}

	// для десктопа
	if(w >= CoffemaniaSharedData.MOBILE_WIDTH) {
		refreshArticleSize();
	} else {
		// console.log("resize");
		resizeMobileMap();
		resizeMobileNowPlaying();
		refreshArticleSize();
	}
});

//
// Всплывание приглашения к установке приложения
//

// function toggleAppPromoBanner (device) {
// 	console.log("app promo show", device);
// }


// утилиты

function toMobile() {
	//
	// console.log("to mobile");
	//$(".iscroll-wrapper").data("iscroll-api").destroy();
	//$(".iscroll-wrapper").data("iscroll-api").options.eventPassthrough = true;
	// $(".iscroll-wrapper").data("iscroll-api").options.scrollY = false;
	// $(".iscroll-wrapper").data("iscroll-api").options.preventDefault = false;

	//$(".iscroll-wrapper").data("iscroll-api").refresh();
	CoffemaniaSharedData.angularScope.$broadcast("toMobile");
	resizeMobileMap();
	resizeMobileNowPlaying();
	$("article").each(function() {
		$("<div class='site-footer'></div>").appendTo($(this)).html($("#site-footer").html());
	});
}

function toDesktop() {
	//
	// console.log("to desktop");
	//createIScroll(".iscroll-wrapper");
	//$(".iscroll-wrapper").data("iscroll-api").enable();
	//$(".iscroll-wrapper").data("iscroll-api").options.eventPassthrough = false;
	// $(".iscroll-wrapper").data("iscroll-api").options.scrollY = true;
	// $(".iscroll-wrapper").data("iscroll-api").options.preventDefault = true;
	//$(".iscroll-wrapper").data("iscroll-api").refresh();
	CoffemaniaSharedData.angularScope.$broadcast("toDesktop");
	$("article[data-hash='players-map']").height("auto");
	resizeNowPlayingArticle();
	$(".site-footer").remove();
}

function refreshArticleSize(height) {
	var h = $(".owl-item.active").height();
	$(".owl-item.active").parents(".owl-height").height(h + (height ? height : 0));
	forceRedraw();
}

function resizeNowPlayingArticle() {
	var line = parseInt(window.getComputedStyle(document.body)["line-height"]);
	var h = parseInt($("body").height() / line) * line;
	var articleHeight = ((h - 18*line)*0.5) + (6*line);
	$("article[data-hash='now-playing']").css({
		"height" : articleHeight + "px",
		"max-height": articleHeight + "px"
	});
}

function resizeMobileMap() {
	var line = parseInt(window.getComputedStyle(document.body)["line-height"]);
	$("article[data-hash='players-map']").height($(window).height() - (line*10));
}

function resizeMobileNowPlaying() {
	var line = parseInt(window.getComputedStyle(document.body)["line-height"]);
	$("article[data-hash='now-playing']").css({
		"height": ($(window).height() - (line*7)) + "px",
		"max-height": ($(window).height() - (line*7)) + "px"
	});
}

// function refreshIScroll() {
// 	$(".iscroll-wrapper").data("iscroll-api").refresh();
// }

function forceRedraw() {
	var content = $("#site-content").eq(0);
	if(navigator.userAgent.search("WebKit") > -1) {
		setTimeout(function() {
			content.get(0).style.visibility = "hidden";
			content.get(0).offsetHeight;
			content.get(0).style.visibility = "visible";
		},450);
	}
}

// function createIScroll(selector) {
// 	// пустые не давать
// 	if($(selector).length < 1) return;

// 	// инициализация
// 	var result = new IScroll(selector, {
// 		scrollbars: true,
// 		mouseWheel: true,
// 		interactiveScrollbars: true,
// 		shrinkScrollbars: 'scale',
// 		fadeScrollbars: true,
// 		click: true
// 	});

// 	// запоминаем апи
// 	$(selector).data("iscroll-api", result);

// 	return result;
// }


