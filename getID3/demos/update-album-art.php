<?

//Скрипт обновляет обложки альбомов

//debug
//$output = shell_exec('ls');

//chdir ( '/home/admin/www/coffeemania.fm/app/webroot/getID3/demos' );

//echo 'CURRENT DIR: '.getcwd() . "\n";
//echo shell_exec('pwd');
//exit(0);

if( $_POST['action'] != 'update' ) { //защита от случайного запуска
	echo 'To update album art, please press the button in admin panel.';
	exit(0);
}


echo 'Updating album art. Please wait. JOB STARTED...<br>';

//удаляем папку media (она могла остаться, если скрипт в предыдущий запуск не завершился)
$output = shell_exec('rm -rf media');

//копируем mp3 файлы с сервера радио в папку media
$output = shell_exec('wget -r -nH -A mp3 ftp://radio:uowDWMH@coffeemania.fm/media');

//удаляем папку album-images_new (она могла остаться, если скрипт в предыдущий запуск не завершился)
$output = shell_exec('rm -rf album-images_new');

//Создать папку album-images_new
$output = shell_exec('mkdir -m 777 album-images_new');

//--------------------------- Extract album art - BEGIN ----------------------------------------
//echo '<br><br>Saving album pictures STARTED...';

set_time_limit ( 10 * 60 ); //10 min

//$rootpath = getcwd();
$rootpath = getcwd() . '/media';

// include getID3() library (can be in a different directory if full path is specified)
require_once('../getid3/getid3.php');

// Initialize getID3 engine
$getID3 = new getID3;

//associative array. key is $artist.$track, value is album image file name 
$albumimage = array();

//create Recursive Directory Iterator
$fileinfos = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootpath)
);
foreach($fileinfos as $pathname => $fileinfo) {
    if (!$fileinfo->isFile()) continue;
	
	// Analyze file and store returned data in $ThisFileInfo
	$ThisFileInfo = $getID3->analyze($pathname);
	getid3_lib::CopyTagsToComments($ThisFileInfo);

	/*
	echo 'artist: ' . $ThisFileInfo['comments']['artist'][0]; //artist
	echo '<br>';
	echo 'track: ' . $ThisFileInfo['comments']['title'][0]; //track
	echo '<br>';
	echo '<hr>';
	flush();
	*/

	//echo $ThisFileInfo['comments']['picture'][0]['data']; //picture

	$key = $ThisFileInfo['comments']['artist'][0] . $ThisFileInfo['comments']['title'][0];

	$imagefile = md5($key) . '.png';

	file_put_contents('album-images_new/'.$imagefile, $ThisFileInfo['comments']['picture'][0]['data']); //save album image

	//add image file name to array
	$albumimage[$key] = $imagefile;

}

file_put_contents('album-images_new/'.'images.dat', serialize($albumimage)); //save serialized $albumimage array

//echo '<br>Saving album pictures FINISHED';
//--------------------------- Extract album art - END ----------------------------------------

//Удалить папку  «album-images»
shell_exec('rm -rf album-images');

//Переименовать папку «album-images_new» в «album-images»
shell_exec('mv album-images_new album-images');

//Удалить папку «media»
shell_exec('rm -rf media');

echo '<br><br>JOB FINISHED. You may close this browser window.';

?>