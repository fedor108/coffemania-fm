<?

echo 'Saving album pictures STARTED...';

set_time_limit ( 10 * 60 ); //10 min

//$rootpath = getcwd();
$rootpath = getcwd() . '/media';

// include getID3() library (can be in a different directory if full path is specified)
require_once('../getid3/getid3.php');

// Initialize getID3 engine
$getID3 = new getID3;

//associative array. key is $artist.$track, value is album image file name 
$albumimage = array();

//create Recursive Directory Iterator
$fileinfos = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootpath)
);
foreach($fileinfos as $pathname => $fileinfo) {
    if (!$fileinfo->isFile()) continue;
	
	// Analyze file and store returned data in $ThisFileInfo
	$ThisFileInfo = $getID3->analyze($pathname);
	getid3_lib::CopyTagsToComments($ThisFileInfo);

echo 'artist: ' . $ThisFileInfo['comments']['artist'][0]; //artist
echo '<br>';
echo 'track: ' . $ThisFileInfo['comments']['title'][0]; //track
echo '<br>';
echo '<hr>';
flush();

	//echo $ThisFileInfo['comments']['picture'][0]['data']; //picture

	$key = $ThisFileInfo['comments']['artist'][0] . $ThisFileInfo['comments']['title'][0];

	$imagefile = md5($key) . '.png';

	file_put_contents('album-images/'.$imagefile, $ThisFileInfo['comments']['picture'][0]['data']); //save album image

	//add image file name to array
	$albumimage[$key] = $imagefile;

}

file_put_contents('album-images/'.'images.dat', serialize($albumimage)); //save serialized $albumimage array

echo '<br>FINISHED';
?>