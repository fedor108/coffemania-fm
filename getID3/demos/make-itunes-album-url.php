<?
/**********************************
Делаем партнерскую ссылку на альбом в iTunes. 
**********************************/

//готовим данные для запроса
$artist = $_POST['artist'];
$track = $_POST['track'];
$country = '';
if( $_POST['country'] == 'en' ){
	$country = 'gb';
}else{
	$country = $_POST['country'];
}

//debug 
//file_put_contents('aaaaaaaaaa.log', $artist.'+'.$track);

//debug
/*
$artist = "FKJ";
$track = "Learn to Fly (feat. Jordan Rakei)";
$country = 'ru';

$artist = "Markus Enochson";
$track = "Take Me Away (feat. Cornelia) [Eva Be's Take Me to Eva Be Remix]";
$country = 'ru';
*/

$queryUrl = "http://itunes.apple.com/search?&media=music&entity=song&country=" . $country . "&term=" . urlencode($artist) . "+" . urlencode($track);

//задаем таймаут
$ctx = stream_context_create(array(
    'http' => array(
        'timeout' => 5 //таймаут в секундах
        )
    )
); 

/* --------------- кеширование запросов к iTunes Search API ---------- BEGIN ------- */
include("phpfastcache/phpfastcache.php");
$cache = phpFastCache("sqlite");
$cacheID = md5($queryUrl); 
$result_json = $cache->get($cacheID); // try to get from Cache first.

//debug
//$logEntry = $queryUrl." [result from cache] \n";

if($result_json == null) {
//debug
//$logEntry = $queryUrl." [result from API call] \n";	
    $result_json = file_get_contents($queryUrl, 0, $ctx); //делаем запрос к iTunes Search API
    $cache->set($cacheID, $result_json, 3600*24); // Write to Cache Save API Calls next time
}

//debug
//file_put_contents('aaaaaaaaaaaaa.log', $logEntry, FILE_APPEND | LOCK_EX);

/* --------------- кеширование запросов к iTunes Search API ---------- END ------- */

//делаем запрос к iTunes Search API (без кеширования)
//$result_json = file_get_contents($queryUrl, 0, $ctx);

//обработка таймаута и другого неудачного ответа
if( $result_json == FALSE ){
	echo 0;
	exit(0);
}

$result_arr = json_decode($result_json, true);

//debug
//echo '<hr>';
//echo $queryUrl;
//echo '<hr>';
//echo '<pre>';
//print_r($result_arr);
//echo '</pre>';

if( !empty($result_arr['results'][0]['trackViewUrl']) ){
	//добавляем 
	echo $result_arr['results'][0]['trackViewUrl'];
	exit(0);
}else{
	echo 0;
	exit(0);	
}



